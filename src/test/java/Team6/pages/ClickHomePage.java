package Team6.pages;

import Team6.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ClickHomePage {
    private static By logo = By.xpath("//a[@href='https://w3schools.com']");
    private WebDriver driver;
    private BasePage basePage;

    public ClickHomePage(WebDriver driver){
        this.driver = driver;
//        basePage = new BasePage(driver);
    }

    public void directHome() {
        basePage.waitforPageLoaded();
        basePage.clickElement(logo);
        basePage.waitforPageLoaded();
    }
}
