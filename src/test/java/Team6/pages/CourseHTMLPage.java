package Team6.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CourseHTMLPage {
    public WebDriverWait wait;

    @FindBy(id = "w3loginbtn") WebElement loginBtn;
    @FindBy(id = "modalusername") WebElement accountInput;
    @FindBy(id = "current-password") WebElement passwordInput;
    @FindBy(className = "_1VfsI") WebElement signInBtn;
    @FindBy(css = "#navigation > a._2rAqd.undefined._MohKV") WebElement myLearningBtn;
    @FindBy(xpath = "//*[@id=\"navigation\"]/a[5]") WebElement HtmlBtn;
    @FindBy(xpath = "//*[@id=\"modal_first_name\"]") WebElement fisrtNameInput;
    @FindBy(css = "#main > div.w3-example > a") WebElement tryBtn;
    @FindBy(css = "#textareawrapper > div > div.CodeMirror-scroll > div.CodeMirror-sizer > div > div > div > div.CodeMirror-code > pre:nth-child(9) > span > span:nth-child(4)")
    WebElement inputHTML;
    @FindBy(id = "runbtn") WebElement runBtn;
    WebDriver driver;
    public CourseHTMLPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);


    }
    public void checkCourse(String account,String password) throws InterruptedException {
        loginBtn.click();
        accountInput.sendKeys(account);
        passwordInput.sendKeys(password);
        signInBtn.click();
        Thread.sleep(5000);

        myLearningBtn.click();
        Thread.sleep(5000);
        HtmlBtn.click();
        fisrtNameInput.clear();
        fisrtNameInput.sendKeys("Tra Nguyen");

    }
}
