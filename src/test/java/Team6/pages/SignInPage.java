package Team6.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignInPage {
    @FindBy(id = "w3loginbtn") WebElement loginBtn;
    @FindBy(id = "modalusername") WebElement accountInput;
    @FindBy(id = "current-password") WebElement passwordInput;
    @FindBy(className = "_1VfsI") WebElement signInBtn;

    WebDriver driver;
    public SignInPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }
    public void signIn(String account,String password) throws InterruptedException {
        loginBtn.click();
        accountInput.sendKeys(account);
        passwordInput.sendKeys(password);
        signInBtn.click();

    }
}
